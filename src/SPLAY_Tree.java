/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/3/14
 * Time: 10:12 AM
 *
 * SPLAY_Tree class inherits from the general tree class and overrides
 * -insert(int)
 * -remove(int)
 * -inorder()
 * Implements a tree balanced by the splaying
 */
public class SPLAY_Tree extends Tree {
    Node root;
    Node needsBalance;

    public SPLAY_Tree() { root=null; needsBalance = null;}

    @Override
    public void insert(int newItem) {
        put(newItem);
        if (needsBalance!=root) splay(needsBalance);
        needsBalance = null;
    }

    @Override
    public void remove(int k) {
        root = remove(k,root);
        if(needsBalance!=null) splay(needsBalance);
        needsBalance = null;
    }

    @Override
    public void inorder() {inorder(root);}

    private Node remove(int k, Node p){
        if (p == null) System.out.println(String.format("No key found with value %d",k));
        if (k < p.key) {
            p.leftChild = remove(k, p.leftChild);
        }
        else if (k > p.key) {
            p.rightChild = remove(k, p.rightChild);
        }
        else if (p.isLeaf()){
            needsBalance = p.parent;
            p.parent = null;
            p = null;
        }
        else if (p.leftChild == null){
            p = p.rightChild;
            p.parent.rightChild = null;
            Node t = p.parent.parent;
            p.parent.parent = null;
            p.parent = t;
            needsBalance = p;
        }
        else if (p.rightChild == null){
            p = p.leftChild;
            p.parent.leftChild = null;
            Node t = p.parent.parent;
            p.parent.parent = null;
            p.parent = t;
            needsBalance = p;
        }
        else{
            p.key = findMin(p.rightChild);
            p.rightChild = remove(p.key, p.rightChild);
        }
        return p;
    }

    private void put(int newItem) {
        if ( root == null ) {
            root = new Node( newItem );
            needsBalance = root;
            return;
        }
        Node tracer = root;
        while (true) {
            if ( newItem < tracer.key ) {
                if ( tracer.leftChild == null ) {
                    tracer.leftChild = new Node( newItem, tracer );
                    needsBalance = tracer.leftChild;
                    return;
                }
                else
                    tracer = tracer.leftChild;
            }
            else {
                if ( tracer.rightChild == null ) {
                    tracer.rightChild = new Node( newItem, tracer );
                    needsBalance = tracer.rightChild;
                    return;
                }
                else
                    tracer = tracer.rightChild;
            }
        }
    }

    private void splay(Node x){
        while (!x.isRoot()){
            Node par = x.parent;
            Node grandParent = par.parent;
            if (grandParent == null){
                if (x.isLeftChild()) rightRotate(par);
                else leftRotate(par);
            } else{
                if (x.isLeftChild()){
                    if (par.isLeftChild()){
                        rightRotate(grandParent);
                        rightRotate(par);
                    } else {
                        rightRotate(par);
                        leftRotate(grandParent);
                    }
                } else {
                    if (par.isLeftChild()) {
                        leftRotate(par);
                        rightRotate(grandParent);
                    } else {
                        leftRotate(grandParent);
                        leftRotate(par);
                    }
                }
            }
        }
        root = x;
    }
}
