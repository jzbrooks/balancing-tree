import java.io.*;

/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/1/14
 * Time: 11:20 AM
 *
 * Main driver class to handle input
 */
public class Main {

    public static void main(String[] args) {
        String tree_type = "", command = "", instruction = "", item = "";
        Tree tree = null;

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        try {
            tree_type = in.readLine();
        } catch (IOException ex) {
            System.out.println("Error while reading tree type");
        }

        if (tree_type.equals("AVL")) { tree = new AVL_Tree(); }
        else if (tree_type.equals("Splay")) { tree = new SPLAY_Tree(); }

        try {

            while ((command = in.readLine()) != null) {
                instruction = item = "";
                for (int i=0; i<command.length(); i++) {
                    if (command.charAt(i)==' ')
                    {
                        instruction = command.substring(0,i).trim();
                        item = command.substring(i+1,command.length()).trim();
                    }
                }

                if (instruction.equals("Insert") && tree!=null) { tree.insert(Integer.parseInt(item));}
                else if (instruction.equals("Remove") && tree!=null) { tree.remove(Integer.parseInt(item));}
                if(tree!=null) tree.inorder();
                System.out.println();
            }

        } catch (IOException ex) {
            System.out.println("Error while reading commands");
        }
    }
}
