/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/1/14
 * Time: 11:40 AM
 *
 * AVL_Tree class inherits from the general tree class and overrides
 * -insert(int)
 * -remove(int)
 * -inorder()
 * Implements a tree balanced by AVL height ordering
 */
public class AVL_Tree extends Tree {
    Node root;

    public AVL_Tree() {
        root = null;
    }

    @Override
    public void insert(int k) {
       root = insert(k, root);
    }

    @Override
    public void remove(int k) { root = remove(k, this.root); }

    @Override
    public void inorder() { inorder(root); }

    private Node insert(int k, Node p) {
        if (p == null)
            return new Node(k);
        if (k < p.key) {
            p.leftChild = insert(k, p.leftChild);
            p.leftChild.parent = p;
        } else if (k > p.key) {
            p.rightChild = insert(k, p.rightChild);
            p.rightChild.parent = p;
        } else
            System.out.println("Duplicate Key");

        return rebalance(p);
    }

    private Node remove(int k, Node p) {
        if (p == null) System.out.println(String.format("No key found with value %d",k));
        if (k < p.key) {
            p.leftChild = remove(k, p.leftChild);
        } else if (k > p.key) {
            p.rightChild = remove(k, p.rightChild);
        } else if (p.rightChild == null)
            p = p.leftChild;
        else if (p.leftChild == null)
            p = p.rightChild;
        else {
            p.key = findMin(p.rightChild);
            p.rightChild = remove(p.key, p.rightChild);
        }
        return rebalance(p);
    }

    private Node rebalance(Node p) {
        if (p==null) return p;

        if (height(p.leftChild) - height(p.rightChild) > 1)
            if(height(p.leftChild.leftChild) >= height(p.leftChild.rightChild))
                p = rightRotate(p);
            else
                p = doubleRotateLeft(p);
        else
            if (height(p.rightChild) - height(p.leftChild) > 1)
                if(height(p.rightChild.rightChild) >= height(p.rightChild.leftChild))
                    p = leftRotate(p);
                else
                    p = doubleRotateRight(p);
        p.height = Math.max(height(p.leftChild), height(p.rightChild)) + 1;
        return p;
    }

    private Node doubleRotateLeft(Node F) {
        F.leftChild = leftRotate(F.leftChild);
        return rightRotate(F);
    }

    private Node doubleRotateRight(Node F) {
        F.rightChild = rightRotate(F.rightChild);
        return leftRotate(F);
    }
}
