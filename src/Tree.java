/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/1/14
 * Time: 11:45 AM
 *
 * General tree class (abstract) meant to be inherited by SPLAY_Tree
 * and AVL_Tree
 *
 * Requires:
 * -insert(int)
 * -remove(int)
 * -inorder()
 */
public abstract class Tree {
    abstract public void insert(int k);
    abstract public void remove(int k);
    abstract public void inorder();

    protected int height(Node p) {
        return (p==null)?-1:p.height;
    }

    protected int findMin(Node p) {
        if (p.leftChild == null) return p.key;
        return findMin(p.leftChild);
    }

    protected void inorder(Node p) {
        if (p==null) return;
        System.out.print("(");
        inorder(p.leftChild);
        if(p.leftChild!=null) System.out.print(" ");
        System.out.print(p.key);
        if(p.rightChild!=null) System.out.print(" ");
        inorder(p.rightChild);
        System.out.print(")");
    }

    // Heights are recalculated, but only the AVL tree rebalances based on height
    protected Node rightRotate(Node B) {
        Node par = B;
        Node gp = B.parent;
        Node D = par.leftChild;
        par.leftChild = D.rightChild;
        D.rightChild = par;
        D.parent = B.parent;
        if(gp!=null && par.isLeftChild()) D.parent.leftChild = D;
        if(gp!=null && par.isRightChild()) D.parent.rightChild = D;
        B.parent = D;
        B.height = Math.max(height(B.leftChild), height(B.rightChild)) + 1;
        D.height = Math.max(height(D.rightChild), D.height) + 1;
        return D;
    }

    protected Node leftRotate(Node B) {
        Node par = B;
        Node gp = B.parent;
        Node D = par.rightChild;
        par.rightChild = D.leftChild;
        D.leftChild = par;
        D.parent = B.parent;
        if(gp!=null && par.isLeftChild()) D.parent.leftChild = D;
        if(gp!=null && par.isRightChild()) D.parent.rightChild = D;
        B.parent = D;
        B.height = Math.max( height(B.leftChild), height(B.rightChild)) + 1;
        D.height = Math.max( height(D.leftChild), B.height) + 1;
        return D;
    }
}
