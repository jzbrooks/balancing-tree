/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/1/14
 * Time: 11:40 AM
 *
 * Binary Tree Node class that carries height information and a parent pointer
 * Methods can be used to determine information about the node
 */
public class Node {
    Node parent, leftChild, rightChild;
    public int key,height;

    public Node(int k) { parent=null; height = 0; this.key = k;}
    public Node(int k, Node par) {parent = par; height= 0; key = k;}

    public boolean isLeftChild() {return !this.isRoot() && this == this.parent.leftChild;}
    public boolean isRightChild() {return !this.isRoot() && this == this.parent.rightChild;}
    public boolean isLeaf() {return this.leftChild==null && this.rightChild==null;}
    public boolean isRoot() {return this.parent == null;}
}
